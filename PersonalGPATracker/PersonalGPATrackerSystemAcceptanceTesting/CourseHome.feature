﻿Feature: Course List Home
	In order to add new course
	As a student
	I want to click add new course to add course

	

@Normal_Flow
Scenario: Click on Add course Link in Home page
	Given I am on the home page
	When I have clicked add new course link
	Then the result should be a redirection to Add New Course page 


@Normal_Flow
Scenario: Calculate GPA
	Given I navigate to add new course page
		  And I have entered CSCI5230 as the code
		  And I have entered Capstone as the title	
	      And I have selected 3 as credit hours
		  And I have selected A as letter grade
	When I press add course
	Then the result should be GPA:4.00 and displayed on the home page
