﻿Feature: DeleteCourse
	In order to delete course information
	As a student
	I want to click on delete button


Background: 
	Given I navigate to add new course page
		  And I have entered CSCI5230 as the code
		  And I have entered Capstone as the title	
	      And I have selected 3 as credit hours
		  And I have selected A as letter grade
	 When I press add course
     


@Normal_FLow
Scenario: Navigate To Delete course Page
	Given I am on the home page
	When I press course delete link
	Then the result should be a redirection to delete page

@Normal_FLow
Scenario: Delete course 
	Given I am on the delete page
	When I press delete course 
	Then the result should be course record deleted and back to home page

@Normal_FLow
Scenario: Back to home from course delete 
	Given I am on the delete page
	When I press back to list 
	Then the result should be back to the home page
