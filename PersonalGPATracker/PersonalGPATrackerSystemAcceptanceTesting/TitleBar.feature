﻿Feature: TitleBarFunctionalities
	In order to check title bar functionalities
	As a student
	I want to click on title bar links

@Normal_Flow
Scenario: Click Add Course Link on Title Bar
	Given I am on the home page
	When I click on Add Course Link on the title bar
	Then the result should be navigated to Add New Course page

@Normal_Flow
Scenario: Click on Home Link on Title Bar
	Given I am on the home page
	When I click on Home Link on the title bar
	Then the result should be redirected to home page