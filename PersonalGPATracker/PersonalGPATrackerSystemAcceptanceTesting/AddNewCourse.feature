﻿Feature: AddNewCourse
	In order to keep track of GPA
	As a student
	I want to add new course information

Background: 
	Given I navigate to add new course page

@Normal_FLow
Scenario: Add New Course
	Given I have entered CSCI5230 as the code
		  And I have entered Capstone as the title	
	      And I have selected 3 as credit hours
		  And I have selected A as letter grade
	When I press add course
	Then the result should be new course information recorded and displayed on the home page

@Exceptions_Flow
Scenario: Invalid course code
	Given I have entered * as the code
	And I have entered V&V as the title
	And I have selected 3 as credit hours
	And I have selected A as letter grade
	When I press add course
	Then the result should be course code is invalid

@Normal_Flow
Scenario: Back To Course List From Add New Course page
	Given I have clicked Back To Course List link
	Then the result should be back to the home page


