﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PersonalGPATrackerTestFramework;


namespace PersonalGPATrackerSystemAcceptanceTesting
{
    [Binding]
    public class DeleteCourseSteps
    {
        // Delete course
        [Given]
        public void GivenIAmOnTheDeletePage()
        {
            DeleteCoursePage.GoTo();
        }

        [When]
        public void WhenIPressDeleteCourse()
        {
            DeleteCoursePage.DeleteCourse();
        }

        [When]
        public void WhenIPressBackToList()
        {
            DeleteCoursePage.BackToHomeList();
        }

        [Then]
        public void ThenTheResultShouldBeCourseRecordDeletedAndBackToHomePage()
        {
            var TableRow = DeleteCoursePage.GetTableRows();
            Assert.That(TableRow, Is.EqualTo(1));
        }


        [Then]
        public void ThenTheResultShouldBeBackToTheHomePage()
        {
            var PageTitle = CourseHome.PageTitle;
            Assert.That(PageTitle, Is.EqualTo("Course List and GPA - My ASP.NET Application"));
        }

        // Navigate to Deleter course
        [When]
        public void WhenIPressCourseDeleteLink()
        {
            DeleteCoursePage.DeleteCourseLink();
        }

        [Then]
        public void ThenTheResultShouldBeARedirectionToDeletePage()
        {
            var PageTitle = DeleteCoursePage.PageTitle;
            Assert.That(PageTitle, Is.EqualTo("Delete course :CSCI5230 - My ASP.NET Application"));
        }


        [Given]
        public void GivenIAmOnTheCourseDetailsPage()
        {
            CourseDetails.GoTo();
        }

        [When]
        public void WhenIPressEditLink()
        {
            CourseDetails.NavigateToCourseEdit();
        }

        [When]
        public void WhenIPressBackToCourseList()
        {
            CourseDetails.BackToCourseListFromCourseDetails();
        }
    }
}
