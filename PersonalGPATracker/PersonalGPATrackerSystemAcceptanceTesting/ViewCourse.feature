﻿Feature: ViewCourse
	In order to see course information
	As a student
	I want to click on course details

Background: 
	Given I navigate to add new course page
		  And I have entered CSCI5230 as the code
		  And I have entered Capstone as the title	
	      And I have selected 3 as credit hours
		  And I have selected A as letter grade
	 When I press add course
     

@Normal_FLow
Scenario: View course
	Given I am on the home page
	When I press course details link
	Then the result should be couse details displayed on the screen

@Normal_Flow
Scenario: Navigate to Edit course
	Given I am on the course details page
	When I press edit link
	Then the result should be navigated to course edit page

@Normal_Flow
Scenario: Back to Course List
	Given I am on the course details page
	When I press back to course list
	Then the result should be navigated course list

