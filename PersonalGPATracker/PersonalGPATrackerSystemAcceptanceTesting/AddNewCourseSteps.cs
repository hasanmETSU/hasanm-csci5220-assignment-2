﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using TechTalk.SpecFlow;
using PersonalGPATrackerTestFramework;

namespace PersonalGPATrackerSystemAcceptanceTesting
{
    [Binding]
    public class AddNewCourseSteps
    {
        

        /// Add New course info
        [Given]
        public void GivenINavigateToAddNewCoursePage()
        {
            AddNewCoursePage.GoTo();
        }

        [Given]
        public void GivenIHaveEntered_CODE_AsTheCode(string code)
        {
            AddNewCoursePage.Code = Convert.ToString(code);
        }

        [Given]
        public void GivenIHaveEntered_TITLE_AsTheTitle(string title)
        {
            AddNewCoursePage.Title = Convert.ToString(title);
        }

        [Given]
        public void GivenIHaveSelected_CREDITHOUR_AsCreditHours(int credithour)
        {
            AddNewCoursePage.CreditHoure = Convert.ToString(credithour);
        }


        [Given]
        public void GivenIHaveSelected_LETTER_AsLetterGrade(string letter)
        {
            AddNewCoursePage.LetterGrade = Convert.ToString(letter);
        }


        [When]
        public void WhenIPressAddCourse()
        {
            AddNewCoursePage.AddCourse();
        }

        [Given]
        public void GivenIHaveClickedBackToCourseListLink()
        {
            AddNewCoursePage.BackToCourseList();
        }


        [Then]
        public void ThenTheResultShouldBeNewCourseInformationRecordedAndDisplayedOnTheHomePage()
        {
            var courseRowInformation = AddNewCoursePage.GetCourseInformataion();
            Assert.That(courseRowInformation, Is.EqualTo("CSCI5230 Capstone 3 A 4 12.0 Edit | Details | Delete"));
        }


        [Then]
        public void ThenTheResultShouldBeCourseCodeIsInvalid()
        {
            string ActualCode = AddNewCoursePage.Code;
            StringAssert.IsMatch("[$&+,:;=?@#|'<>.-^*()%!]", ActualCode,
                "Invalid Course Code");
        }
    }
}
