﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PersonalGPATrackerTestFramework;

namespace PersonalGPATrackerSystemAcceptanceTesting
{
    [Binding]
    public class TitleBarSteps
    {

        [When]
        public void WhenIClickOnAddCourseLinkOnTheTitleBar()
        {          
            TitleBarPage.AddCourseLink();
        }

        [Then]
        public void ThenTheResultShouldBeNavigatedToAddNewCoursePage()
        {
            var PageTitle = TitleBarPage.PageTitle;
            Assert.That(PageTitle, Is.EqualTo("Add New Course - My ASP.NET Application"));
        }

        [When]
        public void WhenIClickOnHomeLinkOnTheTitleBar()
        {
            TitleBarPage.ClickHomeLink();
        }

        [Then]
        public void ThenTheResultShouldBeRedirectedToHomePage()
        {
            var PageTitle = TitleBarPage.PageTitle;
            Assert.That(PageTitle, Is.EqualTo("Course List and GPA - My ASP.NET Application"));
        }


    }
}
