﻿Feature: EditCourse
	In order to update course information
	As a student
	I want to change course information

Background: 
	Given I navigate to add new course page
		  And I have entered CSCI5230 as the code
		  And I have entered Capstone as the title	
	      And I have selected 3 as credit hours
		  And I have selected A as letter grade
	 When I press add course
     

@Normal_FLow
Scenario: Edit a course	
	Given I have navigated to edit page
		  And I have entered CSCI5230 as the code
		  And I have entered Capstone as the title
		  And I have selected 3 as credit hours
		  And I have selected B- as letter grade
	When I press edit button
	Then the result should be edit cousre


@Exception_Flow
Scenario: Empty Course Title
	Given I have navigated to edit page
		  And I have entered CSCI5231 as the code
		  And I have entered   as the title
		  And I have selected 3 as credit hours
		  And I have selected B- as letter grade
	When I press edit button
	Then title is invalid should be displayed
