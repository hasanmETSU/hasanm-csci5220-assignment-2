﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PersonalGPATrackerTestFramework;

namespace PersonalGPATrackerSystemAcceptanceTesting
{
    [Binding]
    public class CourseListHomeSteps
    {
        [Before]
        public static void Setup()
        {
            AddNewCoursePage.Initialize();
        }

        [After]
        public static void TearDown()
        {
            AddNewCoursePage.EndTest();
        }

        [Given]
        public void GivenIAmOnTheHomePage()
        {
            CourseHome.GoTo();
        }

        [When]
        public void WhenIHaveClickedAddNewCourseLink()
        {

            CourseHome.ClickAddNewCourse();
        }

        [Then]
        public void ThenTheResultShouldBeARedirectionToAddNewCoursePage()
        {
            var PageTitle = CourseHome.PageTitle;
            Assert.That(PageTitle, Is.EqualTo("Add New Course - My ASP.NET Application"));
        }

        [Then]
        public void ThenTheResultShouldBeGPA_GPA_AndDisplayedOnTheHomePage(string gpa)
        {
            var totalGPA = CourseHome.TotalGPA;
            var gpap = "Total GPA: " + gpa;
            Assert.That(totalGPA, Is.EqualTo(gpap));
        }

    }
}
