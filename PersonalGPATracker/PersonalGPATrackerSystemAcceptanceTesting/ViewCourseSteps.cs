﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PersonalGPATrackerTestFramework;

namespace PersonalGPATrackerSystemAcceptanceTesting
{
    [Binding]
    public class ViewCourseSteps
    {
        /// Course Details
        [When]
        public void WhenIPressCourseDetailsLink()
        {
            CourseDetails.ViewCourse();
        }

        [Then]
        public void ThenTheResultShouldBeCouseDetailsDisplayedOnTheScreen()
        {
            var courseCode = CourseDetails.GetCourseCode();
            var courseTitle = CourseDetails.GetCourseTitle();

            Assert.That(courseCode, Is.EqualTo("CSCI5230"));
            Assert.That(courseTitle, Is.EqualTo("Capstone"));
        }

        [Then]
        public void ThenTheResultShouldBeNavigatedToCourseEditPage()
        {
            var PageTitle = CourseDetails.PageTitle;
            Assert.That(PageTitle, Is.EqualTo("Editing Course CSCI5230 - My ASP.NET Application"));
        }

        [Then]
        public void ThenTheResultShouldBeNavigatedCourseList()
        {
            var PageTitle = CourseDetails.PageTitle;
            Assert.That(PageTitle, Is.EqualTo("Course List and GPA - My ASP.NET Application"));
        }
    }
}
