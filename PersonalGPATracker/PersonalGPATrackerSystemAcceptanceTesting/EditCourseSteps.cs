﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using PersonalGPATrackerTestFramework;


namespace PersonalGPATrackerSystemAcceptanceTesting
{
    [Binding]
    public class EditCourseSteps
    {
        /// Edit Course
        [Given]
        public void GivenIHaveNavigatedToEditPage()
        {
            EditCoursePage.GoTo();
        }

        [When]
        public void WhenIPressEditButton()
        {
            EditCoursePage.EditCourse();
        }

        [Then]
        public void ThenTheResultShouldBeEditCousre()
        {
            var courseRowInformation = EditCoursePage.GetCourseInformataion();
            Assert.That(courseRowInformation, Is.EqualTo("CSCI5230 Capstone 3 B- 2.7 8.1 Edit | Details | Delete"));
        }

        [Given]
        public void GivenIPressAddCourse()
        {
            AddNewCoursePage.AddCourse();
        }

        [Then]
        public void ThenTitleIsInvalidShouldBeDisplayed()
        {
            String title = EditCoursePage.Title;
            Assert.That(title, Is.Empty);
        }
    }
}
