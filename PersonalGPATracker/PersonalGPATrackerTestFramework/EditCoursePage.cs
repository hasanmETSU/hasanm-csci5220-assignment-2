﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverFramework;

namespace PersonalGPATrackerTestFramework
{
    public static class EditCoursePage
    {
        private const string url = "http://localhost:58316/course/edit/CSCI5230";

        public static string Code
        {
            set { Chrome.Code = value; }
            get { return Chrome.Code; }

        }

        public static string Title
        {
            set { Chrome.Title = value; }
            get { return Chrome.Title; }
        }

        public static string CreditHoure
        {
            set { Chrome.CreditHour = value; }
        }


        public static string LetterGrade
        {
            set { Chrome.LetterGrade = value; }
        }

        public static void Initialize()
        {
            Chrome.Create();
        }

        public static void GoTo()
        {
            Chrome.GoTo(url);
        }

        public static void EndTest()
        {
            Chrome.Quit();
        }

        public static void EditCourse()
        {
            Chrome.EditCourse();
        }

        public static string GetCourseInformataion()
        {
            return Chrome.GetCourseInformataion();
        }
    }
}
