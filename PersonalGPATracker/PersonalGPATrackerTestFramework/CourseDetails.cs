﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverFramework;

namespace PersonalGPATrackerTestFramework
{
    public static class CourseDetails
    {
        private const string url = "http://localhost:58316/course/details/CSCI5230";

        public static string Code
        {
            set { Chrome.Code = value; }
            get { return Chrome.Code; }

        }

        public static string Title
        {
            set { Chrome.Title = value; }
        }

        public static string CreditHoure
        {
            set { Chrome.CreditHour = value; }
        }


        public static string LetterGrade
        {
            set { Chrome.LetterGrade = value; }
        }

        public static string PageTitle
        {
            get
            {
                return Chrome.PageTitle;
            }
        }

        public static void Initialize()
        {
            Chrome.Create();
        }

       public static void EndTest()
        {
            Chrome.Quit();
        }

        public static void ViewCourse()
        {
            Chrome.ViewCourse();
        }

        public static void GoTo()
        {
            Chrome.GoTo(url);
        }

        public static void NavigateToCourseEdit()
        {
            Chrome.NavigateToCourseEdit();
        }

        public static void BackToCourseListFromCourseDetails()
        {
            Chrome.BackToCourseList();
        }

        public static string GetCourseCode()
        {
            return Chrome.GetCourseCode();
        }

        public static string GetCourseTitle()
        {
            return Chrome.GetCourseTitle();
        }
    }
}
