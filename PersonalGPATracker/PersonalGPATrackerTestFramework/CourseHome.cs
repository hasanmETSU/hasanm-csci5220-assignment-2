﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverFramework;

namespace PersonalGPATrackerTestFramework
{
    public static class CourseHome
    {
        private const string url = "http://localhost:58316/";

        public static string PageTitle
        {
            get
            {
                return Chrome.PageTitle;
            }
        }

        public static string TotalGPA
        {
            get
            {
                return Chrome.TotalGPA;
            }
        }

        public static void Initialize()
        {
            Chrome.Create();
        }

        public static void GoTo()
        {
            Chrome.GoTo(url);
        }

        public static void EndTest()
        {
            Chrome.Quit();
        }

        public static void ClickAddNewCourse()
        {
            Chrome.ClickAddNewCourse();
        }

        
    }
}
