﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverFramework;

namespace PersonalGPATrackerTestFramework
{
    public static class TitleBarPage
    {
        private const string url = "http://localhost:58316/";

        public static string PageTitle
        {
            get
            {
                return Chrome.PageTitle;
            }
        }

        public static void AddCourseLink()
        {
            Chrome.AddCourseLinkOnTitleBar();
        }

        public static void ClickHomeLink()
        {
            Chrome.ClickHomeLink();
        }
    }
}
