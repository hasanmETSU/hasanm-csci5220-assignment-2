﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverFramework;

namespace PersonalGPATrackerTestFramework
{
    public static class AddNewCoursePage
    {
        private const string url = "http://localhost:58316/Course/Add";

        public static string Code
        {
            set { Chrome.Code = value; }
            get { return Chrome.Code; }
        }

        public static string Title
        {
            set { Chrome.Title = value; }
        }

        public static string CreditHoure
        {
            set { Chrome.CreditHour = value; }
        }


        public static string LetterGrade
        {
            set { Chrome.LetterGrade = value; }
        }

        public static void Initialize()
        {
            Chrome.Create();
        }

        public static void GoTo()
        {
            Chrome.GoTo(url);
        }

        public static void EndTest()
        {
            Chrome.Quit();
        }

        public static void ClickAddNewCourse()
        {
            Chrome.ClickAddNewCourse();
        }

        public static void AddCourse()
        {
            Chrome.AddCourse();
        }

        public static string GetCourseInformataion()
        {
            return Chrome.GetCourseInformataion();
        }

        public static void BackToCourseList()
        {
            Chrome.BackToCourseListFromAdd();
        }
    }
}
