﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverFramework;

namespace PersonalGPATrackerTestFramework
{
    public static class DeleteCoursePage
    {
        private const string url = "http://localhost:58316/course/delete/CSCI5230";

        public static string Code
        {
            set { Chrome.Code = value; }
            get { return Chrome.Code; }

        }

        public static string PageTitle
        {
            get
            {
                return Chrome.PageTitle;
            }
        }

        public static string Title
        {
            set { Chrome.Title = value; }
        }

        public static string CreditHoure
        {
            set { Chrome.CreditHour = value; }
        }


        public static string LetterGrade
        {
            set { Chrome.LetterGrade = value; }
        }

        public static void Initialize()
        {
            Chrome.Create();
        }

        public static void EndTest()
        {
            Chrome.Quit();
        }

        public static void DeleteCourseLink()
        {
            Chrome.DeleteCourseLink();
        }


        public static void GoTo()
        {
            Chrome.GoTo(url);
        }

        public static void DeleteCourse()
        {

            Chrome.DeleteCourse();
        }

        public static void BackToHomeList()
        {
            Chrome.BackToHomeList();
        }

        public static int GetTableRows()
        {
            return Chrome.GetTableRows();
        }
    }
}
