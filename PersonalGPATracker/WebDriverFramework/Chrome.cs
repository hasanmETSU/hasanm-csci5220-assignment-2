﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace WebDriverFramework
{
    public static class Chrome
    {
        private static IWebDriver _page = null;

        public static string PageTitle
        {
            get
            {
                return _page.Title;
            }
        }

        public static void Create()
        {
            _page = new ChromeDriver(@"C:\Users\sumonETSU\Documents\Visual Studio 2015\chromedriver_win32");
        }

       
        public static string Code
        {
            set
            {
                var codeElement = _page.FindElement(By.CssSelector("input[name=\"Code\"]"));
                codeElement.Clear();
                codeElement.SendKeys(value); 
            }

            get
            {
                return _page.FindElement(By.XPath("/html/body/div[2]/table/tbody/tr[2]/td[1]")).Text;
            }
        }

       

        public static string Title
        {
            set
            {
                var titleElement = _page.FindElement(By.CssSelector("input[name=\"Title\"]"));
                titleElement.Clear();
                titleElement.SendKeys(value);
            }
            get
            {
                return _page.FindElement(By.XPath("/html/body/div[2]/table/tbody/tr[2]/td[2]")).Text;
            }
        }

     
        public static string CreditHour
        {
            set
            {
                var creditHourElement = _page.FindElement(By.XPath("//*[@id=\"CreditHours\"]"));   ///By.CssSelector("input[name=\"CreditHours\"]")
                var creditHourSelect = new SelectElement(creditHourElement);
                creditHourSelect.SelectByText(Convert.ToString(value));
            }
            get
            {
                return _page.FindElement(By.XPath("/html/body/div[2]/table/tbody/tr[2]/td[3]")).Text;
            }
        }

 
        public static string LetterGrade
        {
            set
            {
                var letterGradeElement = _page.FindElement(By.XPath("//*[@id=\"LetterGrade\"]"));
                var letterSelect = new SelectElement(letterGradeElement);
                letterSelect.SelectByText(Convert.ToString(value));
            }
        }

        

        public static void GoTo(string url)
        {
            _page.Navigate().GoToUrl(url);
        }

        public static void Quit()
        {
            _page.Dispose();
            _page.Quit();
        }

        public static void ClickAddNewCourse()
        {
            _page.FindElement(By.XPath("/html/body/div[2]/p/a")).Click();
        }

        public static void AddCourse()
        {
            _page.FindElement(By.XPath("/html/body/div[2]/form/div/div[5]/div/input")).Click();
        }

        public static void EditCourse()
        {
            _page.FindElement(By.XPath("/html/body/div[2]/form/div/div[5]/div/input")).Click();
        }

        public static void ViewCourse()
        {
            _page.FindElement(By.XPath("/html/body/div[2]/table/tbody/tr[2]/td[7]/a[2]")).Click();
        }

        public static void DeleteCourseLink()
        {
            _page.FindElement(By.XPath("/html/body/div[2]/table/tbody/tr[2]/td[7]/a[3]")).Click();
        }

        public static void DeleteCourse()
        {
            _page.FindElement(By.XPath("/html/body/div[2]/div/form/div/input[2]")).Click();
        }

        public static void BackToHomeList()
        {
            _page.FindElement(By.XPath("/html/body/div[2]/div/form/div/a")).Click();
        }

        public static void NavigateToCourseEdit()
        {
            _page.FindElement(By.XPath("/html/body/div[2]/p/a[1]")).Click();
        }

        public static void BackToCourseList()
        {
            _page.FindElement(By.XPath("/html/body/div[2]/p/a[3]")).Click();
        }

        public static int GetTableRows()
        {
           
            return _page.FindElements(By.XPath("/html/body/div[2]/table/tbody/tr")).Count;
        }

        public static string GetCourseTitle()
        {
            return _page.FindElement(By.XPath("/html/body/div[2]/div/dl/dd[2]")).Text;
        }

        public static string GetCourseCode()
        {
            return _page.FindElement(By.XPath("/html/body/div[2]/div/dl/dd[1]")).Text;
        }

        public static string GetCourseInformataion()
        {
            var course =  _page.FindElement(By.XPath("/html/body/div[2]/table/tbody/tr[2]")).Text;
            return course;
        }

        public static string TotalGPA
        {
            get
            {
                return _page.FindElement(By.XPath("/html/body/div[2]/h1")).Text;
            }
        }

        public static void AddCourseLinkOnTitleBar()
        {
            _page.FindElement(By.XPath("/html/body/div[1]/div/div[2]/ul/li[2]/a")).Click();
        }

        public static void ClickHomeLink()
        {
            _page.FindElement(By.XPath("/html/body/div[1]/div/div[2]/ul/li[1]/a")).Click();
        }

        public static void BackToCourseListFromAdd()
        {
            _page.FindElement(By.XPath("/html/body/div[2]/div/a")).Click();
        }

    }
}
